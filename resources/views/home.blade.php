@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <task-list></task-list>
        </div>

    </div>
</div>
@endsection

@section('footer_js')
    <script>
        $(document).ready(function () {
            console.log('ready');
        });
    </script>
@endsection
