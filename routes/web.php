<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'TaskController@index')->name('task.index');
Route::get('tasks', 'TaskController@tasks')->name('task.index');
Route::post('tasks', 'TaskController@store')->name('task.store');
Route::post('tasks/update/{id}', 'TaskController@update')->name('task.update');
Route::post('tasks/done/{id}', 'TaskController@setDone')->name('task.done');
Route::post('tasks/undone/{id}', 'TaskController@setUnDone')->name('task.undone');
Route::get('tasks/{id}', 'TaskController@destroy')->name('task.destroy');
