<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('home');
    }

    public function tasks()
    {
        return Auth::user()->tasks()->orderBy('created_at', 'desc')->get();
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'text' => 'required|max:500'
        ]);
        $task = Task::create([ 'text' => $request->text, 'user_id' => Auth::id() ]);

        return $task;
    }

    public function update(Request $request, $id)
    {
        $task = Task::find($id)->update([ 'text' => $request->text, 'user_id' => Auth::id()]);
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return 204;
    }

    public function setDone($id)
    {
        $task = Task::findOrFail($id);
        $task->done = 1;
        $task->save();
        return 204;
    }

    public function setUnDone($id)
    {
        $task = Task::findOrFail($id);
        $task->done = 0;
        $task->save();
        return 204;
    }
}

